import logging
from typing import List, Optional

import requests
from django.conf import settings
from django.db.models import Model
from pydantic import BaseModel, Field

from stork.utils import queryset_iterator

logger = logging.getLogger(__name__)


class Contact(BaseModel):
    email: str
    tags: dict
    register_timestamp: float
    data_timestamp: float
    kladr_id: Optional[str] = None


class CanaryData(BaseModel):
    secret_key: str
    contacts: List[Contact]


class CanaryResponse(BaseModel):
    success_emails: List[str] = Field(alias='success')
    errors: dict


def send_to_canary(canary_data: CanaryData) -> CanaryResponse:
    logger.debug(f'request payload >> {settings.USERS_SYNC_URL} payload: {canary_data.dict()}')
    response = requests.post(settings.USERS_SYNC_URL, json=canary_data.dict())
    logger.debug(f'response text << {response.text}')
    response.raise_for_status()
    return CanaryResponse(**response.json())


def sync_model(target_model: type(Model), chunk_size: int = 1000):
    queryset = target_model.objects.unsynced()
    for start, end, total, chunk in queryset_iterator(queryset, chunk_size):
        payload = CanaryData(
            secret_key=settings.USERS_SYNC_SECRET_KEY,
            contacts=chunk.to_canary(),
        )
        canary_response = send_to_canary(canary_data=payload)
        if canary_response.success_emails:
            logger.info(
                f'Successfully sent chunk {start}-{end} of {total}. '
                f'Synced emails count: {len(canary_response.success_emails)}'
            )

            target_model.objects.filter(email__in=canary_response.success_emails).update(is_synchronized=True)

        if canary_response.errors:
            logger.warning(f'Errors on sync: {canary_response.errors}')
        logger.info('Chunk synchronized.')

    logger.info('Done')

