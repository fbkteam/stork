from django.apps import apps
from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from stork.models import CanarySyncAbstractModel
from stork.services import sync_model


class Command(BaseCommand):
    help = 'Отправка данных пользователей в канарейку.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--target-model', metavar='app_label.ModelName', dest='target_model',
            help='Модель, которую необходимо синхронизировать в виде app_label.ModelName',
        )
        parser.add_argument(
            '--chunk-size', default=1000, dest='chunk_size', type=int,
            help='Количество обрабатываемых записей за одну итерацию',
        )

    def handle(self, *args, **options):
        target_model_label = options['target_model']
        chunk_size = options['chunk_size']

        if not all([getattr(settings, 'USERS_SYNC_URL', None), getattr(settings, 'USERS_SYNC_SECRET_KEY', None)]):
            self.stdout.write(self.style.WARNING('USERS_SYNC_URL or USERS_SYNC_SECRET_KEY not define in settings'))
            return

        app_label, model_label = target_model_label.split('.')
        try:
            app_config = apps.get_app_config(app_label)
        except LookupError as e:
            raise CommandError(str(e))
        try:
            target_model = app_config.get_model(model_label)
        except LookupError:
            raise CommandError("Unknown model: %s.%s" % (app_label, model_label))

        if not issubclass(target_model, CanarySyncAbstractModel):
            self.stdout.write(self.style.WARNING('Model should be subclass of CanarySyncAbstractModel'))
            return

        sync_model(target_model=target_model, chunk_size=chunk_size)
