from typing import List

from django.db import connection, models
from django.db.models.base import ModelBase
from django.test import TestCase

from .models import CanarySyncAbstractModel, CanarySyncQuerySet


class TestCanarySyncQuerySet(CanarySyncQuerySet):
    def to_canary(self) -> List[dict]:
        persons_values = self.values_list(
            'email', 'created_at', 'modified_at', 'kladr_id', named=True
        )

        contacts = []

        for person in persons_values:
            data = {
                'email': person.email,
                'kladr_id': person.kladr_id,
                'tags': {},
                'register_timestamp': person.created_at.timestamp(),
                'data_timestamp': person.modified_at.timestamp(),
            }
            contacts.append(data)

        return contacts


class CanarySyncTestCase(TestCase):
    model = CanarySyncAbstractModel

    def setUp(self):
        self.model = ModelBase(
            '__TestModel__' + self.model.__name__, (self.model,),
            {
                '__module__': self.model.__module__,
                'email': models.EmailField(null=True, blank=True),
                'kladr_id': models.CharField(max_length=150, null=True, blank=True),
                'fields_to_canary_resync': ['email', 'kladr_id'],
                'objects': TestCanarySyncQuerySet.as_manager()
            }
        )

        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(self.model)

    def tearDown(self):
        with connection.schema_editor() as schema_editor:
            schema_editor.delete_model(self.model)

    def test_model_fields(self):
        """Проверяем наличие необхиодмых полей для синхронизации"""
        model_instance = self.model.objects.create()

        self.assertTrue(hasattr(model_instance, 'fields_to_canary_resync'))
        self.assertTrue(hasattr(model_instance, 'is_synchronized'))
        self.assertTrue(hasattr(model_instance, 'created_at'))
        self.assertTrue(hasattr(model_instance, 'modified_at'))
        self.assertTrue(hasattr(model_instance, 'email_confirmed'))

    def test_fields_to_canary_resync(self):
        """Проверяем сброс флага синхронизации при изменении полей"""
        model_instance = self.model.objects.create()
        self.assertEqual(model_instance.fields_to_canary_resync, ['email', 'kladr_id'])

        # Отмечаем как синхронизированную
        model_instance.is_synchronized = True
        model_instance.save()
        self.assertTrue(model_instance.is_synchronized)

        # Меняем поле, которое должно привести к сбросу флага
        model_instance.email = 'test@gmail.com'
        model_instance.save()

        # Проверяем, что флаг сбросился
        self.assertFalse(model_instance.is_synchronized)

    def test_to_canary_queryset_method(self):
        model_data = dict(email='example@gmail.com', kladr_id='123123123123123')

        model_instance = self.model.objects.create(**model_data)
        to_canary_result = self.model.objects.to_canary()
        keys_in_result = {'email', 'kladr_id', 'tags', 'register_timestamp', 'data_timestamp'}

        first_result = to_canary_result[0]
        self.assertEqual(set(first_result.keys()), keys_in_result)

        for field_name in model_data.keys():
            self.assertEqual(first_result[field_name], getattr(model_instance, field_name))
