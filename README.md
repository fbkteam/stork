# Синхронизация с канарейкой

В рамках пакета реализованы:

1. Абстрактная модель с необходимыми для синхронизации полями
2. Queryset для нее, с методами, необходимыми для вполнения команды синхронизации
3. manage.py команда синхронизации

## Установка

    pip install git+https://bitbucket.org/fbkteam/stork.git

## Настройка

Добавить stork в INSTALLED_APPS:

    INSTALLED_APPS = [
        ...
        'stork',
    ]

В настройки проекта необходимо добавить 2 переменные:

    USERS_SYNC_URL - url канарейки для запросов
    USERS_SYNC_SECRET_KEY - код приложения для доступа

Опционально можно задать путь к публичному ключу для проверки SSL:

    USERS_SYNC_SSL_VERIFY = '/path/to/public_key.pem'

Или отключить проверку SSL:

    USERS_SYNC_SSL_VERIFY = False

Для подключения модели к синхронизации необходимо

1. Унаследовать модель от класса абстрактной модели
2. Определить имплементацию метода to_canary queryset-а CanarySyncQuerySet 


    from stork.models import CanarySyncAbstractModel, CanarySyncQuerySet


    class PersonCanarySyncQuerySet(CanarySyncQuerySet):
        def to_canary(self):
            contacts = []
            for person in self:
                data = {
                    'email': person.email,
                    'tags': {},
                    'register_timestamp': person.created_at.timestamp(),
                    'data_timestamp': person.modified_at.timestamp(),
                    'kladr_id': person.city__kladr_id or person.city__region__kladr_id
                }
                contacts.append(data)
    
            return contacts
    
    
    class Person(CanarySyncAbstractModel):
        fields_to_canary_resync = ['email', 'city']
        ...
        
        objects = PersonCanarySyncQuerySet.as_manager()


В fields_to_canary_resync можно указать список полей, обновление которых приведет
к повторной синхронизации записи при следующем запуске команды.


После этого можно запускать manage.py команду синхронизации:

    ./manage.py canary_sync --target-model some_app.Person --sleep 5

